"use strict";




var myApp = angular.module('myApp', ['ui.router']);
 myApp.config(function($stateProvider, $urlRouterProvider) {
 $urlRouterProvider.otherwise("/state1");
 
   $stateProvider
    .state('state1', {
      url: "/state1",
      templateUrl: "app/js/lib/template/state1.html"
    });
 }

    